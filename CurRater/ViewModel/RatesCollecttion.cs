﻿using CurRater.Common;
using CurRater.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurRater.ViewModel
{
    public class RatesCollecttion
    {
        public async Task<Dictionary<string, CurRate>> InitializeRates(string to, params Currency[] currencies)
        {
            try
            {
                var rates = await RequestRates(to, currencies).ConfigureAwait(false);

                return new Dictionary<string, CurRate>
                {
                    { "EUR", new CurRate(Currency.EUR, rates.EUR_AMD) },
                    { "GBP", new CurRate(Currency.GBP, rates.GBP_AMD) },
                    { "USD", new CurRate(Currency.USD, rates.USD_AMD) },
                    { "RUB", new CurRate(Currency.RUB, rates.RUB_AMD) }
                };
            }
            catch
            {

                throw;
            }
        }

        public async Task<Rates> RequestRates(string to, params Currency[] currencies)
        {
            var httpClent = new HttpClient();
            var result = new List<CurRate>();
            var response = new HttpResponseMessage();

            try
            {
                string requestPairs = $"{currencies[0].ToString()}_{to}";
                for (int i = 1; i < currencies.Length; i++)
                    requestPairs += $",{currencies[i].ToString()}_{to}";

                response = await httpClent.GetAsync($"https://free.currconv.com/api/v7/convert?q={requestPairs}&compact=ultra&apiKey=22f98423af86e99d6f12").ConfigureAwait(false);
                var serializedData = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Rates>(serializedData);
            }
            catch
            {
                throw;
            }
        }
    }
}
