﻿using CurRater.Common;
using CurRater.ViewModel;

namespace CurRater.Models
{
    public class CurRate : BaseModel
    {
        #region Private Fields

        private decimal _balance;
        private Currency _currency;

        #endregion

        #region Properties

        public decimal Balance
        {
            get => _balance;
            set => Set(ref _balance, value);
        }

        public Currency Currency
        {
            get => _currency;
            set => Set(ref _currency, value);
        }

        #endregion

        #region Constructors

        public CurRate() => _balance = 1M;

        public CurRate(Currency currency, decimal balance = 1M)
        {
            Currency = currency;
            Balance = balance;
        }

        #endregion
    }
}