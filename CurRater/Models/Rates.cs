﻿namespace CurRater.Models
{
    public class Rates
    {
        public decimal EUR_AMD { get; set; }
        public decimal GBP_AMD { get; set; }
        public decimal USD_AMD { get; set; }
        public decimal RUB_AMD { get; set; }
    }
}
