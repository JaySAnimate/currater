﻿namespace CurRater.Common
{
    public enum Currency
    {
        GBP,
        EUR,
        USD,
        AMD,
        RUB
    }
}