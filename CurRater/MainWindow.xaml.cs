﻿using CurRater.Common;
using CurRater.Models;
using CurRater.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace CurRater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Fields

        private string _to = "AMD";
        private string _defaultCurrencyUnit = "1";

        private RatesCollecttion RColViewModel;
        private Dictionary<string, CurRate> _curRates;

        private Currency[] _currencies =
        {
            Currency.EUR,
            Currency.GBP,
            Currency.USD,
            Currency.RUB,
        };

        #endregion

        #region Constructors

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                _curRates = new Dictionary<string, CurRate>();
                for (int i = 0; i < _currencies.Length; i++)
                {
                    _curRates.Add(_currencies[i].ToString(), new CurRate(_currencies[i]));
                    _curRates[_currencies[i].ToString()].PropertyChanged += UpdateUICurrencies;
                }

                RColViewModel = new RatesCollecttion();
                BeginInitializingRates();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region View Events

        private void X_Click(object sender, RoutedEventArgs e) => Close();

        private void __Click(object sender, RoutedEventArgs e) => WindowState = WindowState.Minimized;

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        #endregion

        #region UIFunctions

        private void BeginInitializingRates()
        {
            try
            {
                Task.Run(async () => await GetRates(null, null));

                TimeSpan span = TimeSpan.FromHours(2);
                Timer timer = new Timer
                {
                    Interval = span.TotalMilliseconds,
                    AutoReset = true
                };

                timer.Elapsed += async (sender, args) => await GetRates(sender, args);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task GetRates(object sender, ElapsedEventArgs args)
        {
            Dictionary<string, CurRate> rates = null;
            await Dispatcher.Invoke(async () =>
            {
                rates = await RColViewModel.InitializeRates(_to, _currencies);

                _curRates["EUR"].Balance = rates["EUR"].Balance;
                _curRates["GBP"].Balance = rates["GBP"].Balance;
                _curRates["USD"].Balance = rates["USD"].Balance;
                _curRates["RUB"].Balance = rates["RUB"].Balance;
            });
        }

        private void UpdateUICurrencies(object sender, PropertyChangedEventArgs args)
        {
            LEURamd_balance.Content = _defaultCurrencyUnit;
            LGBPamd_balance.Content = _defaultCurrencyUnit;
            LUSDamd_balance.Content = _defaultCurrencyUnit;
            LRUBamd_balance.Content = _defaultCurrencyUnit;

            LeurAMD_balance.Content = _curRates["EUR"].Balance.ToString("C");
            LgbpAMD_balance.Content = _curRates["GBP"].Balance.ToString("C");
            LusdAMD_balance.Content = _curRates["USD"].Balance.ToString("C");
            LrubAMD_balance.Content = _curRates["RUB"].Balance.ToString("C");
        }

        #endregion
    }
}